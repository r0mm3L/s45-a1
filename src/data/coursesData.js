

const coursesData = [
    {
        id: "wdc001",
        name: "PHP-Laravel",
        description: "learn PHP-Laravel in no time",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Java- Springboot",
        description: "learn Java- Springboot in no time",
        price: 60000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Python-Django",
        description: "learn Python-Django in no time",
        price: 60000,
        onOffer: true
    }
]

export default coursesData