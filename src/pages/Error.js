// import React from 'react';
// import { Link } from 'react-router-dom';

// const Error = () =>
//   <div>
//     <h3>Zuitt booking</h3>
//     <p>Page not Found</p>
//     <span>Go back to</span><a href="http://localhost:3000/">Home</a>
//   </div>

// export default Error;


import Banner from '../component/Banner';

export default function Error() {

    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }
    
    return (
        <Banner data={data}/>
    )
}