import { Form , Button} from "react-bootstrap";
import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import UserContext from '../UserContext';
import { Redirect } from "react-router-dom";
import Swal from 'sweetalert2'
import { useHistory } from "react-router-dom";

export default function Register() {

    const history = useHistory();
    const { user, setUser } = useContext(UserContext);
    // state hooks to store the values of the input fields
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [mobileno, setMobileno] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [email, setEmail] = useState('')

    // state to determine whether register button is enabled or not
    const [isActive, setIsActive] = useState(false)



    function registeruser(e) {

        e.preventDefault();

//

fetch('http://localhost:4000/users/register',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        firstName: firstname,
        lastName: lastname,
        mobileNo: mobileno,
        email: email,
        password: password1
    })

})	
.then(res => res.json())
.then(data => {
console.log(data)

if(data == true){
    

    Swal.fire({
        title: 'Registration Successful!',
        icon: 'success',
        text: 'You can now login'
    })
    
    
} else {

    Swal.fire({
        title: 'Email might be already taken or there is an error in registration',
        icon: 'error',
        text: 'Please try again'
    })
}
})

//

        setEmail('');
        setPassword1('');
        setPassword2('');
        setFirstname('');
        setLastname('');
        setMobileno('');
       

        setTimeout(() => history.push("/login"), 1.5*1000);


    }

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '' && firstname !== '' && lastname !== '' &&  mobileno !== '') 
        
        && (password1 === password2))
        
        {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [email, password1, password2])
    
    // [email, password1, password2])

    // const { user, setUser } = useContext(UserContext);
    // setUser({
    //     loggedEmail:localStorage.getItem('email')
    // })

    return (
        (user.id != null) ?
        <Redirect to="/courses" />
        :

        <Form className="p-5" onSubmit={(e=>registeruser(e))}> 
            
            <Form.Group>
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type = 'string'
                    placeholder = 'Please enter your First Name here'
                    value = {firstname}
                    onChange = {e => setFirstname(e.target.value)}
                    required

                />
                
            </Form.Group>

            <Form.Group>
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type = 'string'
                    placeholder = 'Please enter your Last Name here'
                    value = {lastname}
                    onChange = {e => setLastname(e.target.value)}
                    required

                />
                
            </Form.Group>
            
            <Form.Group>
                <Form.Label>Email Address</Form.Label>
                <Form.Control
                    type = 'email'
                    placeholder = 'Please enter your email here'
                    value = {email}
                    onChange = {e => setEmail(e.target.value)}
                    required

                />
                <Form.Text className = "text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type = 'string'
                    placeholder = 'Please enter your Mobile Number here'
                    value = {mobileno}
                    onChange = {e => setMobileno(e.target.value)}
                    required

                />
                
            </Form.Group>

            <Form.Group controlId = "password1">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                    type = 'password'
                    placeholder = 'Please input your password here'
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                    />
            </Form.Group>

                <Form.Group controlId = 'password2'>
                    <Form.Label>Verify Password:</Form.Label>
                    <Form.Control
                        type = 'password'
                        placeholder = 'please verify your password'
                        value = {password2}
                    onChange = {e => setPassword2(e.target.value)}
                        required
                     />
                </Form.Group>

                { isActive ?
                    
                    <Button className="mt-3" variant = 'primary' type = 'submit' id = 'submitBtn'>
                
                Register
                
                </Button>

                :
                
                <Button className="mt-3" variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
                
                Register
                
                </Button>
                
                }
        </Form>
    )
}


// ternary operator with the button tag

// ? - if -- if <button isactive, the color will be primary and it is anabled
// : - else -- else <button will be