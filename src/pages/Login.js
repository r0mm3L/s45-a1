import { Form , Button} from "react-bootstrap";
import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import UserContext from '../UserContext';
import { Redirect } from "react-router-dom";
import Swal from 'sweetalert2'

export default function Login() {

    // allow us to use the User Context object and it's properties to use for user validation.
    const { user, setUser } = useContext(UserContext);
    // state hooks to store the values of the input fields
    const [password1, setPassword1] = useState('')
    const [email, setEmail] = useState('')

    // state to determine whether register button is enabled or not
    const [isActive, setIsActive] = useState(false)

    console.log(email)
    console.log(password1)


    function loginUser(e) {
        // prevents page redirection via form submission
        e.preventDefault();

        fetch('http://localhost:4000/users/login',{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password1
                })
        
        })	
        // .then(res => res.json())
        // .then(data => {
        //     console.log(data)
                
        //         if(typeof data.access !== undefined){
        //             localStorage.setItem('token', data.access)
        //             retrieveUserDetails(data.access)
        //         }
        //     })

        .then(res => res.json())
        .then(data => {
            console.log(data)
    
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token', data.access)
                retrieveUserDetails(data.access)
    
                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt'
                })
                
            } else {
    
                Swal.fire({
                    title: 'Authentication Failed.',
                    icon: 'error',
                    text: 'Check your login details'
                })
            }
        })
    

        // set the email of the user in the local storage
        //syntax:
            // localStorage.setItem('propertyName', value)
  

        // this is to render the email. 


        // clear input fields after submission
        setEmail('');
        setPassword1('');
    
        //window.location.reload();
        //alert('You are Logged in')
   
    }

    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
                console.log(data)

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
        })
    }

    useEffect(() => {
        if(email !== '' && password1 !== '') 
        
        
        
        {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [email, password1])

    return (
        (user.id !== null) ?
            <Redirect to="/courses" />
            :
        <Form className="p-5" onSubmit={(e=>loginUser(e))}> 
            <Form.Group>
                <Form.Label>Email Address:</Form.Label>
                <Form.Control
                    type = 'email'
                    placeholder = 'Please enter your email here'
                    value = {email}
                    onChange = {e => setEmail(e.target.value)}
                    required

                />
                
            </Form.Group>

            <Form.Group controlId = "password1">
                <Form.Label>Password:</Form.Label>
                <Form.Control
                    type = 'password'
                    placeholder = 'Please input your password here'
                    value = {password1}
                    onChange = {e => setPassword1(e.target.value)}
                    required
                    />
            </Form.Group>


                { isActive ?
                    
                    <Button className="mt-3" variant = 'primary' type = 'submit' id = 'submitBtn'>
                
                Login
                
                </Button>

                :
                
                <Button className="mt-3" variant = 'danger' type = 'submit' id = 'submitBtn' disabled>
                
                Login
                
                </Button>
                
                }
                
        </Form>
    )
}
