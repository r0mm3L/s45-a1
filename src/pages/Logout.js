import { Redirect } from "react-router-dom";
import { useContext } from "react";
import { useEffect } from "react";
import UserContext from '../UserContext';


export default function Logout(){
    // use the usercontext object and destructure  it to access the user state and unsetuser function from context provider
    const { user, setUser, unsetUser } = useContext(UserContext);
    // localStorage.clear();

    // clear the localstorage of the users information
    unsetUser();

    // placing the setuser setter function inside the useeffect is necessary because of updates within reactjs that a state of another
    // component cannot be updatd while trying to render a different component
    //by adding the useeffect, this will allow the lgoout page to render first before triggering the use effect which changes the state 
    //of the user
    useEffect( () => {
        // set the user state back to its original value
        setUser({id:null});
    })


    return (
        
        <Redirect to='/login' />

    )
}

