// import { Fragment } from "react"
// import Banner from "../component/Banner"
// // import Error from "../component/Banner"
// import Highlights from "../component/Highlights"
// // import CourseCard from "../component/CourseCard"



// export default function Home(){
//     return(
//         <Fragment>
//             <Banner/>
          
//             <Highlights/>
//             {/* <CourseCard/> */}
//         </Fragment>
//     )
// }


import {Fragment} from 'react';
import Banner from '../component/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../component/Highlights';


export default function Home (){

	const data={
		title: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/",
		label: "Enroll now!"
	}


	return(
		<Fragment>
			<Banner data={data} />
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
}