import coursesData from '../data/coursesData'
import CourseCard from '../component/CourseCard'
import { Fragment } from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
// import coursesData from '../data/coursesData'

export default function Courses(){
    //console.log(coursesData)

    const [courses, setCourses] = useState([])

// const courses = coursesData.map(course =>{
//     return (
//         <CourseCard key = {course.id} courseProp = {course}/>
//     )
// })
// useEffect(() => {
// 	fetch('http://localhost:4000/courses/all')

// 	.then(res => res.json())
// 	.then(data => {
// 		console.log(data)
	
// 	setCourses(data.map(course => {
// 	return (
// 		<CourseCard key = {course.id} courseProp = {course}/>
// 	)
// }))


// 	})
// }, [])

useEffect(() => {
	fetch('http://localhost:4000/courses/all', {
		headers: {
			Authorization: `Bearer ${localStorage.getItem("token")}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setCourses(data.map(course => {
	return (
		<CourseCard key = {course.id} courseProp = {course}/>
	)
}))


	})
}, [])


    return(
        <Fragment>
        <h1>Courses</h1>
        {courses}
        </Fragment>
        
    )
}