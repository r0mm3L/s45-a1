import React from 'react';

// a context is a special react object which allow us to store information within and pass it around our components within our app
// the context object is a different aproach to passing information between components without the 
//need to pass props from component to component. 
const UserContext = React.createContext();


// the provider component allows other components to consume/use the context object and 
//supply the necesary information needed to the context object.
export const UserProvider = UserContext.Provider


export default UserContext;