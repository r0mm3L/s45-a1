import { Card } from "react-bootstrap";
import {Row, Col, Button} from 'react-bootstrap';
import { useState } from "react";
import { Link } from 'react-router-dom';
import { useEffect } from "react";
import { counter } from "react";
import { setCounter } from "react";
import { incrementRate } from "react";

export default function CourseCard({courseProp}){


	console.log(courseProp)

// object destructuring
	const { name, description, price, _id} = courseProp
	// syntax: const {properties} = propname

	// array destructuring
	// const [count, setCount] = useState(0)
	// syntax: const [getter, settings] = useState(initialvalue)

	// console.log(useState(0))

	// function enroll(){
	// 	setCount(count + 1);
	// 	if(count == 30 ){
	// 		alert('no more seats')
	// 		setCount(count + 0)
	// 	}
	// }
		
	
		
    return(
        <Row className = "mt-3 mb-3">
		<Col>
			<Card className = "cardHighlight p-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>.
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					{/* <Card.Text>Enrollees: {count}</Card.Text> */}
					{/* <Card.Text><Button variant = "danger" onClick={() => setCount(count + 1)}> Enroll</Button></Card.Text> */}
					{/* <Card.Text><Button variant = "danger" onClick={enroll}> Enroll</Button></Card.Text> */}
					<Link className = "btn btn-primary" to= {`/courses/${_id}`}>Details</Link>
				</Card.Body>
			</Card>
		</Col>
	</Row>

    )
}