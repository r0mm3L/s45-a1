import './App.css';
import AppNavbar from './component/AppNavbar';
import { useState } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom'
// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import { useEffect } from 'react';
import Home from './pages/Home';
import CourseView from './pages/CourseView';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/Error'


function App() {

  // state hook for the user state that's define here for a global scope
  // initialised as an object with properties from the localStorage
  //this will beused to store the information and will be used for validating if a user logged in on the app or not


  const [user, setUser] = useState({

    id: null,
    isAdmin: null
  })
// function for clearing localStorage
const unsetUser = () => {
  localStorage.clear();
}

useEffect(() => {

  console.log(user)
  console.log(localStorage)

}, [user])

// useEffect(() => {
//   let token = localStorage.getItem('token');
//   fetch('http://localhost:4000/user/details', {
//     method: "GET",
//     headers: {
//       Authorization: `Bearer ${token}`
//     }
//   })
//   .then(res => res.json())
//   .then(data => {
//     console.log(data)
//     if(typeof data._id !== "undefined") {
//       setUser({
//         id:data._id,
//         isAdmin: data.isAdmin
//       })
//     } else {
//       setUser({
//         id: null,
//         isAdmin: null
//       })
//     }
//   })
// }, [])

// the userprovider component is what allows other components to consume or use our context. any component which is not wrapped
// by UserProvider will not have access to the values provided for our context.

// you can pass data or information to our context by providing a value attribute in our UserProvider. 
//data passed here can be accessed by other components by unwrapping our context using the useContext hook.
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar/>
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/courses/:courseId" component={CourseView}/>
            <Route exact path="/courses" component={Courses}/>
            <Route exact path="/courseview" component={CourseView}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/logout" component={Logout}/>
            {/* <Route exact path="/*" component={Error}/> */}
            <Route path="*" component={NotFound} />
            </Switch>
        </Container>
    
    </Router>
    </UserProvider>
  );
}

export default App;

//  1. Router - wrapping the router component around other components will allow us to use routing within our page.
// 2. Swith - Allow us to switch/ change our page components
// 3. Route - assigns a path which will trigger the change/switch of components render.